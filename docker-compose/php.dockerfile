FROM php:8-apache

# install node
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get update && apt-get install -y git zip nodejs zlib1g-dev libpng-dev libfreetype-dev libjpeg-dev libwebp-dev

# install yarn
RUN npm install --global yarn

RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp;
RUN docker-php-ext-install bcmath pdo_mysql gd
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN a2enmod rewrite

CMD ["apache2-foreground"]
